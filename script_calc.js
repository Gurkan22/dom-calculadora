

let cont = 1
var notas = []

function enviar_nota() {
    let input = document.querySelector(".input_nota");
    if (input.value == "" ) {
        alert("Por favor, insira uma nota.")
        
    }
    else if (input.value >=0 && input.value <= 10) {
        let escopo = document.querySelector("#visor");
        let texto = document.createElement("p");
        texto.innerText = "A nota " + cont + " foi " + input.value;
        cont++
        escopo.append(texto);
        notas.push(parseFloat(input.value))
    }
    else {
        alert("A nota digitada é inválida, por favor, insita uma nota válida.")
    }
}

function calcular_nota() {
    let resposta = document.querySelector("#resposta")
    var soma = 0
    for (var i = 0; i < notas.length; i++) {
        soma += notas[i] 
    }
    total = soma / notas.length;
    resposta.innerText = total.toFixed(2);
}

let btn_add =document.querySelector("#btn_add")
btn_add.addEventListener("click",enviar_nota)

let btn_cal = document.querySelector("#btn_cal");
btn_cal.addEventListener("click",calcular_nota)
